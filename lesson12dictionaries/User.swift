import Foundation

class User{
    var name: String?
    var age: Int?
    
    init(name: String? = nil, age: Int? = nil){
        self.name = name
        self.age = age
    }
}
