import UIKit

class ViewController: UIViewController {
    
    //MARK: - Globals
    var user = User()
    var dictionary: [String : Any] = [
        "0075" : User(name: "Alex", age: 20),
        "0076" : User(name: "Jhon", age: 31),
        "0" : 5
    ]
    //MARK: - IBOutlets
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var printButton: UIButton!
    
    //MARK: - Main functions
    override func viewDidLoad() {
        super.viewDidLoad()
        print()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let user = dictionary["0075"] as? User,
            let name = user.name ,
            let age = user.age {
            print("name = \(name)")
            print("age = \(age)")
            print(dictionary)
        }
        print(dictionary)
        print("dev")
    }
    
    //MARK: - IBActions
    @IBAction func addButtonTapped(_ sender: UIButton) {
        dictionary["Key"] = User(name: "Tom", age: 11)
        dictionary["0078"] = "Something"
        dictionary["0075"] = "asdasdasdasd"
        let array = ["1", "2", "3"]
        dictionary["0099"] = array
        let dictionary2: [String: Any] = ["22": User(name: "Paul", age: 28), "28": "asdasdasd"]
        let dictionary3: [String: Any] = ["key1": "sfofofofofo", "key2": User(name: "Lee", age: 19)]
        let secondArray = [dictionary2, dictionary3]
        dictionary["arrayKey"] = secondArray
        print()
    }
    
    @IBAction func printButtonTapped(_ sender: UIButton) {
        print(dictionary)
    }
    //MARK: - Flow functions
    
}

